# Drone basics  
  
## Transmitter(tx) - radiocomando  
la trasmissione e' sui 2.4Ghz, protocollo FrSky   
verificare che sia MODE 2 ("gas" a sinistra)  
sui taranis gira un firmware opensource, opentx:  https://www.open-tx.org/  

- **Taranis QX7:  ~90 euro**  
abbastanza economico ma comunque buono, ha meno switch ma non ne servono cosi' tanti  
https://www.banggood.com/FrSky-ACCST-Taranis-Q-X7-Transmitter-2_4G-16CH-White-Black-International-Version-p-1196246.html?p=H0111815076480201710  
  
- **Taranis X9D:  ~160 euro**  
e' "lo standard" dei tx (io ho questo)  
https://www.banggood.com/Original-FrSky-2_4G-16CH-ACCST-Taranis-X9D-Plus-Transmitter-Carton-Package-p-1121799.html?p=H0111815076480201710
  
- **Taranis X9D SE:  ~200 euro**    
versione con gymbal upgradati e skin figa (tornando indietro prenderei questo)  
https://www.banggood.com/FrSky-2_4G-16CH-Taranis-X9D-Plus-SE-Transmitter-SPECIAL-EDITION-w-M9-Sensor-Water-Transfer-Case-p-1134370.html?p=H0111815076480201710  

con questi trasmettitori e' necessario avere droni che utilizzano il protocollo FrSky  
  
in alternativa e' possibile aggiungere un **modulo 4in1 multiprotocol** per supportare molti dei protocolli piu' comuni:  
https://www.banggood.com/IRangeX-IRX4-Plus-2_4G-CC2500-NRF24L01-A7105-CYRF6936-4-IN-1-Multiprotocol-STM32-TX-Module-With-Case-p-1225080.html?p=H0111815076480201710  
  
funziona con il seguente firmware: https://github.com/pascallanger/DIY-Multiprotocol-TX-Module  


## Goggles  
la ricezione video e' analogica (meno latenza di wifi e simili) sui 5.8Ghz
  
### Box goggles
e' un pannello lcd in una scatola con davanti una lente. sono molto economici e si vede molto bene, l'immagine e' molto grande e immersiva. 
hanno il difetto dell'essere ingombranti e scomodi da portare in giro ma consentono di iniziare spendendo poco.   
per poter registrare i video sull'SD serve che abbiano il DVR. tutti quelli elencati ce l'hanno  
  
- **Eachine EV800D: ~80e**  
abbastanza buono e si puo' staccare il pannello per usarlo come LCD, ma non comodissimo (ho iniziato con questo)  
https://www.banggood.com/Eachine-EV800D-5_8G-40CH-Diversity-FPV-Goggles-5-Inch-800480-Video-Headset-HD-DVR-Build-in-Battery-p-1180354.html?p=H0111815076480201710

- **Eachine VR D2 Pro: ~65e**  
dovrebbe essere piu' comodo del precedente ma non si stacca il pannello  
https://www.banggood.com/Eachine-VR-D2-Pro-Upgraded-5-Inches-800480-40CH-5_8G-Diversity-FPV-Goggles-with-DVR-Lens-Adjustable-p-1137808.html?p=H0111815076480201710  
  
### Binocular goggles
sono molto piu' leggeri e trasportabli, ma anche piu' costosi. la qualita' costruttiva e' nettamente superiore rispetto ai box anche per via della fascia di prezzo.  

- **Fat Shark Dominator Hd3 core: ~330 euro**  
sono il top dei visori. bisogna aggiungere la ricevente, le antenne, le batterie, e il caricabatteria  
https://www.banggood.com/Fatshark-Dominator-HD3-Core-3D-FPV-Goggles-with-HDMI-DVR-Support-Head-Tracker-p-1248759.html?p=H0111815076480201710  

- **Aomway Commander: ~250 euro**  
sono recensiti molto bene ovunque. hanno lo schermo 16:9 e si vede un po' piu' piccolo degli HD3, ma alcuni preferiscono.  
bisogna aggiunere battery case, batterie, caricabatteria  
https://www.banggood.com/Aomway-Commander-Goggles-V1-2D-3D-40CH-5_8G-FPV-Video-Headset-Support-HDMI-DVR-Headtracker-p-1107684.html?p=H0111815076480201710  
battery case:   
https://www.banggood.com/Fatshark-7_4V-18650-Li-ion-Cell-Battery-Case-DC5_52_5-For-FPV-Goggles-Video-Headset-without-Battery-p-1133893.html?p=H0111815076480201710  

## Tiny whoop drone  
  
- **Tiny 6X: ~65e**
usa motori 7x16mm, va bene per volare in casa. con advanced version batterie e caricabatterie  
https://www.banggood.com/KINGKONG-TINY6-65mm-Micro-FPV-Quacopter-With-615-Brushed-Motors-Baced-on-F3-Brush-Flight-Controller-p-1134635.html?p=H0111815076480201710

- **BETA75/BETA75S: ~100e**
un po' piu' grande e stabile, molto robusto, meglio se lo si vuole usare all'aperto  
https://www.amazon.it/BETAFPV-Beta75S-Whoop-Quadcopter-Receiver/dp/B078GM1QVH/ref=sr_1_3?ie=UTF8&qid=1522096474&sr=8-3&keywords=beta75

**scegliere sempre versione frsky**
vanno comprati ricambi per i motori perche' durano ~4 ore prima di consumarsi e diventare inutilizzabili
  
tutti i link di banggood sono col codice di affiliazione, se li usate pagate uguale e mi fate un favore



